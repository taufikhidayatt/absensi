<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Document</title>
</head>
<style>
header{
    position:absolute;
    width:100%;
    z-index: 2;
}
.cards{
    border-radius:20px;
    position:relative;
    font-size:14px
}
.images::before{
    content:"";

}
.view-100{
    height:100vh;
}
.contentHide{
    display:none;
}
.bg-grey{
    background-color: #e5e5e5;
}
</style>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#"><img src="images/bootstrap.png" width="150px" alt="" srcset=""></a>
            <div class="collapse navbar-collapse" id="navbarNav" style="min-width:150px">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><h3 class="m-0">Kementerian Sosial</h2></a>
                    </li>
                </ul>
            </div>
            <a class="navbar-brand" href="#"><img src="images/bootstrap.png" width="150px" alt="" srcset=""></a>
        </nav>
    </header>
    <section class="container-fluid view-100 pt-4">
        <div class="row view-100">
            <div id="div1" class="col-lg-12 py-4 bg-grey contentHide" style="display:block">
                <h3 class="mt-3">page 1</h3>
                <div class="row">
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-1">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div2" class="col-lg-12 py-4 bg-grey contentHide">
                <h3 class="mt-3">page 2</h3>
                <div class="row">
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-1">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div3" class="col-lg-12 py-4 bg-grey contentHide">
                <h3 class="mt-3">page 3</h3>
                <div class="row">
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-1">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mb-3">
                        <div class="card w-100 cards text-center pt-4">
                            <img class="mx-auto" height="200px" width="150px" src="images/pas-foto.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title d-block border-bottom pb-2"><span class="mr-2">1.</span> Raden Hidayat</h5>
                                <p class="card-text">Direktur Utama</p>
                                <p class="card-text">01321809845765</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        firstMove()
    })

    function firstMove(){
        setTimeout(function(){
            page2()
        }, 5000);
    }

    function page2(){
        $('.contentHide').hide()
        $('#div2').fadeIn('8000')
        setTimeout(function(){
            back3()
        }, 5000);
    }

    function back3(){
        $('.contentHide').hide()
        $('#div3').fadeIn('8000')
        setTimeout(function(){
            backPage1()
        }, 5000);
    }

    function backPage1(){
        $('.contentHide').hide()
        $('#div1').fadeIn('8000')
    }
</script>
</html>